import java.util.Scanner;

public class TicTacToeGame {

    public static void main(String[] args) {
        System.out.println("Welcome to Tic Tac Toe! Have fun :)");

        Board board = new Board();
        boolean gameOver = false;
        int player = 1;
        Square playerToken = Square.X;

        Scanner scanner = new Scanner(System.in);

        while (!gameOver) {

            // Print the board board
            System.out.println(board);

            // Set player token
            if (player == 1) {
                playerToken = Square.X;
            } else {
                playerToken = Square.O;
            }

            // Get user input
            System.out.println("Player " + player + ", enter row (1-3), press enter, THEN enter column(1-3) to place your " + playerToken + ": ");
            int row = scanner.nextInt()-1;
            int col = scanner.nextInt()-1;

            // Place token on board
            while (!board.placeToken(row, col, playerToken)) {
                System.out.println("Invalid move. Try again.");
                row = scanner.nextInt()-1;
                col = scanner.nextInt()-1;
            }

            // Check for winner or tie
            if (board.checkIfFull()) {
                System.out.println("It's a tie!");
                gameOver = true;
            } else if (board.checkIfWinning(playerToken)) {
                System.out.println("Player " + player + " wins!");
                gameOver = true;
            } else {
                player++;
                if (player > 2) {
                    player = 1;
                }
            }
			
        }
		//Turns out, you need to close your scanner after using it, I didn't know that!
			scanner.close();
    }

}