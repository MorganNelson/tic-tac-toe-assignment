public enum Square{
		BLANK("_"),
		X("X"),
		O("O");
		private String squareState;
		private Square(String squareState){
			this.squareState = squareState;
		}
		public String toString(){
			return this.squareState;
		}
}
